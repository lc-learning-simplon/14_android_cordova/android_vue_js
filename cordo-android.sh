#!/bin/bash

# Installer Cordova et Gradle
sudo npm install -g cordova
sudo apt-get install gradle

# Installer et activer Java 8
sudo apt-get install openjdk-8-jdk
sudo update-alternatives --config java
sudo update-alternatives --config javac

# Télécharger et dézipper Android Sdk tools
mkdir ~/.android
cd ~/.android
wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
unzip sdk-tools-linux-4333796.zip

# Modifier le PATH (pour toujours)
echo 'export PATH=$PATH:$HOME/.android/tools:$HOME/.android/tools/bin' >> ~/.bashrc

# Modifier le PATH (pour cette session)
PATH=$PATH:$HOME/.android/tools:$HOME/.android/tools/bin

# Installer Android 28
sdkmanager --licenses
sdkmanager "platform-tools" "platforms;android-28"
sdkmanager "build-tools;28.0.3"
